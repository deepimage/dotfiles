#!/bin/bash

# check for installed applications
if ! type git &>/dev/null; then
	echo "Git is NOT Installed"
	exit 1
fi


echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo -e "\tInstalling Dotfiles"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

#--------------------------------------
if ! test -d ~/.bash; 
then
	echo "* bash folder created"
	mkdir ~/.bash
fi

echo "* bash color settings updated"
cp colors.bash ~/.bash
cp prompt-solarized-dark.bash ~/.bash

if ! grep -q "source ~/.bash/colors.bash" ~/.bashrc; 
then
	echo "* bash settings updated"
	echo "source ~/.bash/colors.bash" >> ~/.bashrc
fi

#--------------------------------------
echo "* vim settings updated"
cp .vimrc ~/

# create vim folders
if ! test -d ~/.vim;
then
	mkdir ~/.vim
	mkdir ~/.vim/backups
	mkdir ~/.vim/swaps
	mkdir ~/.vim/undo
	mkdir ~/.vim/bundle
	mkdir ~/.vim/colors
fi

# get color scheme
echo "* downloading vim color scheme"
curl -s https://raw.githubusercontent.com/tomasr/molokai/master/colors/molokai.vim > ~/.vim/colors/molokai.vim

# install NEOBundle
echo "* downloading vim plugins mamanger"
if test -d ~/.vim/bundle;
then
	rm -Rf ~/.vim/bundle
	mkdir ~/.vim/bundle
fi

git clone https://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim &> /dev/null

echo "* installing vim plugins"
# cp plugins file to vim folder
cp plugins.vim ~/.vim

# install plugins
vim +NeoBundleInstall +qall

echo "* install completed"

