" load plugins from vundle
source ~/.vim/plugins.vim

set encoding=utf-8
set nocompatible

set backspace=indent,eol,start

set t_Co=256
set background=dark
colorscheme molokai
syntax on 

" always show tab line
set showtabline=2
" always show status line
set laststatus=2

set tabstop=4
set shiftwidth=4

set clipboard=unnamed

set ttimeoutlen=0
set backupdir=~/.vim/backups
set directory=~/.vim/swaps
set undodir=~/.vim/undo
" enable tmp file for crontab
set backupskip=/tmp/*,/private/tmp/*

set history=1000
" enable mouse for normal and insert modes only; disables selection of text with mouse
set mouse=ni
set noerrorbells
set nowrap
set nu
set title
set undofile
set ttimeoutlen=0

" Unite config
noremap <C-f> :Unite line grep:.<cr>

" default search ignore case
set ignorecase
" search contains uppercase then search using case
set smartcase

" tab line color scheme
hi TabLineFill		ctermfg=0 ctermbg=0
hi TabLineSel		ctermfg=253 ctermbg=68 cterm=none
hi TabLine			ctermfg=240 ctermbg=0 cterm=none

" AirLine settings
let g:airline_powerline_fonts = 1
let g:airline_theme='molokai'
let g:airline_left_sep=''
let g:airline_left_alt_sep=''
let g:airline_right_sep=''
let g:airline_right_alt_sep=''

" exclude files
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.LCK
set wildignore+=*.bmp,*.gif,*.png,*.jpg,*.jpeg,*.psd,*.pdf
set wildignore+=*.avi,*.mov,*.m4v,*.mp4,*.mkv,*.mp3,*.m4a,*.flac
set wildignore+=*.xls,*.xlsx,*.doc,*.docx,*.ppt,*.pptx
set wildignore+=*/WEB-INF/*,*/bower_components/*,*/node_modules/*

" ctrlP settings
let g:ctrlp_show_hidden = 1
let g:ctrlp_working_path_mode = 'rc'
" http://superuser.com/questions/390011/fuzzy-find-within-file-in-vim
let g:ctrlp_cmd = 'CtrlPLastMode'
let g:ctrlp_extensions = ['buffertag', 'tag', 'line', 'dir']

" define FileType
" override coldfusion filetype /usr/local/Cellar/vim/7.4.826/share/vim/vim74
au BufNewFile,BufRead *.cfm		setf cf
au BufNewFile,BufRead *.cfc		setf javascript

" remove trailing whitespace in file on save
autocmd FileType markdown,html,javascript,css,sass,cf,ruby autocmd BufWritePre <buffer> :%s/\s\+$//e

" show mixed-indent whitespace
highlight ExtraWhitespace ctermbg=240
match ExtraWhitespace /^\t*\zs \+/

