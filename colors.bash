source ~/.bash/prompt-solarized-dark.bash

# set 'setgid' color to black instead of default yellow background
LS_COLORS="sg=40"
export LS_COLORS

