" Note: Skip initialization for vim-tiny or vim-small.
" http://www.badassvim.com/install-plugins-in-vim-neobundle/
" To Run: vim +NeoBundleInstall +qall
if 0 | endif

if &compatible
set nocompatible               " Be iMproved
endif

" Required:
set runtimepath^=~/.vim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
NeoBundle "https://github.com/davejlong/cf-utils.vim"
NeoBundle "https://github.com/kien/ctrlp.vim"
NeoBundle "https://github.com/gabesoft/vim-ags"
" NeoBundle "tpope/vim-fugitive"
NeoBundle "https://github.com/Shougo/unite.vim"
NeoBundle "https://github.com/pangloss/vim-javascript"
NeoBundle 'https://github.com/vim-airline/vim-airline'
NeoBundle 'https://github.com/vim-airline/vim-airline-themes'

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck
